USE [master]
GO
/****** Object:  Database [OrderManagemetSystem]    Script Date: 04-01-2021 21:48:46 ******/
CREATE DATABASE [OrderManagemetSystem]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'OrderManagemetSystem', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\OrderManagemetSystem.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'OrderManagemetSystem_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\OrderManagemetSystem_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [OrderManagemetSystem] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [OrderManagemetSystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [OrderManagemetSystem] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET ARITHABORT OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [OrderManagemetSystem] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [OrderManagemetSystem] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET  DISABLE_BROKER 
GO
ALTER DATABASE [OrderManagemetSystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [OrderManagemetSystem] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [OrderManagemetSystem] SET  MULTI_USER 
GO
ALTER DATABASE [OrderManagemetSystem] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [OrderManagemetSystem] SET DB_CHAINING OFF 
GO
ALTER DATABASE [OrderManagemetSystem] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [OrderManagemetSystem] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [OrderManagemetSystem] SET DELAYED_DURABILITY = DISABLED 
GO
USE [OrderManagemetSystem]
GO
/****** Object:  Table [dbo].[tblOrderInfo]    Script Date: 04-01-2021 21:48:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblOrderInfo](
	[orderId] [int] IDENTITY(1,1) NOT NULL,
	[userName] [varchar](50) NOT NULL,
	[emailId] [varchar](50) NOT NULL,
	[address] [varchar](max) NOT NULL,
	[userType] [varchar](30) NOT NULL,
	[productId] [int] NOT NULL,
	[orderStatus] [varchar](30) NOT NULL,
 CONSTRAINT [PK_tblOrderInfo] PRIMARY KEY CLUSTERED 
(
	[orderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblOrderStatusTypes]    Script Date: 04-01-2021 21:48:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblOrderStatusTypes](
	[statusID] [int] IDENTITY(1,1) NOT NULL,
	[statusName] [varchar](30) NULL,
 CONSTRAINT [PK_tblOrderStatusTypes] PRIMARY KEY CLUSTERED 
(
	[statusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblProduct]    Script Date: 04-01-2021 21:48:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblProduct](
	[productId] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](max) NULL,
	[weight] [varchar](50) NULL,
	[height] [varchar](50) NULL,
	[image] [varchar](max) NULL,
	[barcode] [nvarchar](50) NULL,
	[availableQuantity] [int] NULL,
	[colour] [varchar](50) NULL,
	[price] [decimal](18, 2) NULL,
 CONSTRAINT [PK_tblProduct] PRIMARY KEY CLUSTERED 
(
	[productId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblUserRole]    Script Date: 04-01-2021 21:48:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserRole](
	[userRoleId] [varchar](50) NOT NULL,
	[roleName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblUserRole] PRIMARY KEY CLUSTERED 
(
	[userRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblOrderInfo] ON 

INSERT [dbo].[tblOrderInfo] ([orderId], [userName], [emailId], [address], [userType], [productId], [orderStatus]) VALUES (2, N'Prashanth', N'raj@gmail.com', N'gayatrinagar', N'Buyer', 1, N'Placed')
INSERT [dbo].[tblOrderInfo] ([orderId], [userName], [emailId], [address], [userType], [productId], [orderStatus]) VALUES (3, N'RajKing', N'raghuRam@gmail.com', N'HYD', N'Buyer', 4, N'Placed')
SET IDENTITY_INSERT [dbo].[tblOrderInfo] OFF
SET IDENTITY_INSERT [dbo].[tblOrderStatusTypes] ON 

INSERT [dbo].[tblOrderStatusTypes] ([statusID], [statusName]) VALUES (1, N'Placed')
INSERT [dbo].[tblOrderStatusTypes] ([statusID], [statusName]) VALUES (2, N'Approved')
INSERT [dbo].[tblOrderStatusTypes] ([statusID], [statusName]) VALUES (3, N'Cancelled')
INSERT [dbo].[tblOrderStatusTypes] ([statusID], [statusName]) VALUES (4, N'In Delivery')
INSERT [dbo].[tblOrderStatusTypes] ([statusID], [statusName]) VALUES (5, N'Completed')
SET IDENTITY_INSERT [dbo].[tblOrderStatusTypes] OFF
SET IDENTITY_INSERT [dbo].[tblProduct] ON 

INSERT [dbo].[tblProduct] ([productId], [name], [weight], [height], [image], [barcode], [availableQuantity], [colour], [price]) VALUES (1, N'Product P1', N'500l', N'12ft', NULL, NULL, 2, N'Black', CAST(500.00 AS Decimal(18, 2)))
INSERT [dbo].[tblProduct] ([productId], [name], [weight], [height], [image], [barcode], [availableQuantity], [colour], [price]) VALUES (2, N'Product P2', N'100ml', N'1ft', NULL, NULL, 6, N'yellow', CAST(100.00 AS Decimal(18, 2)))
INSERT [dbo].[tblProduct] ([productId], [name], [weight], [height], [image], [barcode], [availableQuantity], [colour], [price]) VALUES (3, N'Product P3', N'250l', N'120ft', NULL, NULL, 10, N'orange', CAST(700.00 AS Decimal(18, 2)))
INSERT [dbo].[tblProduct] ([productId], [name], [weight], [height], [image], [barcode], [availableQuantity], [colour], [price]) VALUES (4, N'Product P4', N'1000l', N'100ft', NULL, NULL, 7, N'Purpule', CAST(4500.00 AS Decimal(18, 2)))
INSERT [dbo].[tblProduct] ([productId], [name], [weight], [height], [image], [barcode], [availableQuantity], [colour], [price]) VALUES (5, N'Product P5', N'5000l', N'250ft', NULL, NULL, 15, N'Red', CAST(5000.00 AS Decimal(18, 2)))
INSERT [dbo].[tblProduct] ([productId], [name], [weight], [height], [image], [barcode], [availableQuantity], [colour], [price]) VALUES (6, N'Product P6', N'500ml', N'50ft', NULL, NULL, 20, N'White', CAST(50.00 AS Decimal(18, 2)))
INSERT [dbo].[tblProduct] ([productId], [name], [weight], [height], [image], [barcode], [availableQuantity], [colour], [price]) VALUES (7, N'Product P7', N'1250l', N'1200ft', NULL, NULL, 50, N'Black', CAST(2000.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[tblProduct] OFF
INSERT [dbo].[tblUserRole] ([userRoleId], [roleName]) VALUES (N'1', N'Administrator')
INSERT [dbo].[tblUserRole] ([userRoleId], [roleName]) VALUES (N'2', N'Buyer')
/****** Object:  StoredProcedure [dbo].[UDP_GetOrderDetails]    Script Date: 04-01-2021 21:48:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Prashanth Raju
-- Create date: 03-01-2021
-- Description:	Below Procedure is to return order details based on user type .
--@Includeallorder Param is used in order to get all order details if @buyername value is Admin.
-- =============================================
CREATE PROCEDURE [dbo].[UDP_GetOrderDetails]( @buyername varchar(50) = null)
                          AS
                          BEGIN
                    
              SET NOCOUNT ON;
           
  
                      Declare @Includeallorder bit = 0;
					  --Declare @buyername varchar(50),@Includeallorder bit=0;
--Set @buyername='Administrator';
           
                      set @Includeallorder = iif(@buyername = 'Administrator', 1, 0);
                      SELECT O.[orderId]
                ,O.[userName]
                ,O.[emailId]
                ,O.[address]
                ,O.[userType]
                ,P.[productId]
  	            ,P.[name]
                ,P.[weight]
                ,P.[height]
                ,P.[image]
                ,P.[barcode]
                ,P.[availableQuantity]
                ,P.[colour]
                ,P.[price]
                ,O.[orderStatus]
            FROM[OrderManagemetSystem].[dbo].[tblOrderInfo] O with(nolock) join
            [OrderManagemetSystem].[dbo].[tblProduct] P with(nolock) on p.productId=O.productId
             Where  (([userName] != iif(@Includeallorder= 1, @buyername,null)) or([userName]= iif (@Includeallorder = 0, @buyername, null)) ) END

GO
USE [master]
GO
ALTER DATABASE [OrderManagemetSystem] SET  READ_WRITE 
GO
