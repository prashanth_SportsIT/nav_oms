﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nav_OMS.Models
{
    public class OrderDetails
    {
        public int orderId { get; set; }
        public string userName { get; set; }
        public string emailId { get; set; }
        public string address { get; set; }
        public string userType { get; set; }
        public int productId { get; set; }
        public string name { get; set; }
        public string weight { get; set; }
        public string height { get; set; }
        public string image { get; set; }
        public string barcode { get; set; }
        public int availableQuantity { get; set; }
        public string colour { get; set; }
        public decimal price { get; set; }
        public string orderStatus { get; set; }

        //public string 
    }
}
