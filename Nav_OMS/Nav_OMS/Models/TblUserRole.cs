﻿using System;
using System.Collections.Generic;

namespace Nav_OMS.Models
{
    public partial class TblUserRole
    {
        public string UserRoleId { get; set; }
        public string RoleName { get; set; }
    }
}
