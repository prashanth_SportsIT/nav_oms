﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Nav_OMS.Models
{
    public partial class OrderManagemetSystemContext : DbContext
    {
        public OrderManagemetSystemContext()
        {
        }

        public OrderManagemetSystemContext(DbContextOptions<OrderManagemetSystemContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblOrderInfo> TblOrderInfo { get; set; }
        public virtual DbSet<TblOrderStatusTypes> TblOrderStatusTypes { get; set; }
        public virtual DbSet<TblProduct> TblProduct { get; set; }
        public virtual DbSet<TblUserRole> TblUserRole { get; set; }
        public virtual DbQuery<OrderDetails> OrderDetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                ///<summary>
                ///This is not required as we are assign connection string in startup class .
                ///</summary>

                //optionsBuilder.UseSqlServer("Data Source=****;Database=OrderManagemetSystem;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblOrderInfo>(entity =>
            {
                entity.HasKey(e => e.OrderId);

                entity.ToTable("tblOrderInfo");

                entity.Property(e => e.OrderId).HasColumnName("orderId");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .IsUnicode(false);

                entity.Property(e => e.EmailId)
                    .IsRequired()
                    .HasColumnName("emailId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrderStatus)
                    .IsRequired()
                    .HasColumnName("orderStatus")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ProductId).HasColumnName("productId");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("userName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserType)
                    .IsRequired()
                    .HasColumnName("userType")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblOrderStatusTypes>(entity =>
            {
                entity.HasKey(e => e.StatusId);

                entity.ToTable("tblOrderStatusTypes");

                entity.Property(e => e.StatusId).HasColumnName("statusID");

                entity.Property(e => e.StatusName)
                    .HasColumnName("statusName")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblProduct>(entity =>
            {
                entity.HasKey(e => e.ProductId);

                entity.ToTable("tblProduct");

                entity.Property(e => e.ProductId).HasColumnName("productId");

                entity.Property(e => e.AvailableQuantity).HasColumnName("availableQuantity");

                entity.Property(e => e.Barcode)
                    .HasColumnName("barcode")
                    .HasMaxLength(50);

                entity.Property(e => e.Colour)
                    .HasColumnName("colour")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Height)
                    .HasColumnName("height")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .IsUnicode(false);

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Weight)
                    .HasColumnName("weight")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblUserRole>(entity =>
            {
                entity.HasKey(e => e.UserRoleId);

                entity.ToTable("tblUserRole");

                entity.Property(e => e.UserRoleId)
                    .HasColumnName("userRoleId")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasColumnName("roleName")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            //modelBuilder.Entity<OrderDetails>(entity => { entity.HasNoKey(); }); ;
        }
    }
}
