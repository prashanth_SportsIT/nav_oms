﻿using System;
using System.Collections.Generic;

namespace Nav_OMS.Models
{
    public partial class TblProduct
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Weight { get; set; }
        public string Height { get; set; }
        public string Image { get; set; }
        public string Barcode { get; set; }
        public int? AvailableQuantity { get; set; }
        public string Colour { get; set; }
        public decimal? Price { get; set; }
    }
}
