﻿using System;
using System.Collections.Generic;

namespace Nav_OMS.Models
{
    public partial class TblOrderStatusTypes
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
}
