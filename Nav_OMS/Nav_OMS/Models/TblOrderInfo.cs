﻿using System;
using System.Collections.Generic;

namespace Nav_OMS.Models
{
    public partial class TblOrderInfo
    {
        public int OrderId { get; set; }
        public string UserName { get; set; }
        public string EmailId { get; set; }
        public string Address { get; set; }
        public string UserType { get; set; }
        public int ProductId { get; set; }
        public string OrderStatus { get; set; }
    }
}
