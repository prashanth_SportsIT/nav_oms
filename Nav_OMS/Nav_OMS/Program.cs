﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Nav_OMS
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var config = Utility.GetConfig();
            CreateWebHostBuilder(args, config).Run();
        }

        public static IWebHost CreateWebHostBuilder(string[] args, IConfigurationRoot config) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel()
                .UseUrls(Convert.ToString(config["WebsiteURL"]))
                .UseDefaultServiceProvider(options => options.ValidateScopes = false)
                .Build();
    }
}


