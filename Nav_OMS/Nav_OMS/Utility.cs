﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Nav_OMS
{
    public class Utility
    {
        public static string strappconfig = "appsettings.json";
        public static IConfigurationRoot GetConfig()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile(strappconfig);
            var configuration = builder.Build();
            return configuration;
        }
    }
}
