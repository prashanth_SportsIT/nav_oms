﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Nav_OMS.Models;

namespace Nav_OMS.Controllers
{
    [Route("api/OrderManagment")]
    [ApiController]
    public class OrderManagmentController : ControllerBase
    {
        private readonly OrderManagemetSystemContext _context;

        public OrderManagmentController(OrderManagemetSystemContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Method To Get all the Products list details.
        /// </summary>
        /// <returns></returns>
        [Route("GetProductList")]
        [HttpGet]
        public List<TblProduct> GetProductList()
        {
            List<TblProduct> lstProdDetails = new List<TblProduct>();
            lstProdDetails = _context.TblProduct.ToList();
            return lstProdDetails;
        }


        /// <summary>
        /// Method to add an order .
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        [Route("AddOrder")]
        [HttpPost]
        public TblOrderInfo AddOrder([FromBody]TblOrderInfo order)
        {
            if (order != null)
            {
                _context.TblOrderInfo.Add(order);
                _context.SaveChanges();

            }
            return order;

        }

        /// <summary>
        /// Method to delete the order .
        /// </summary>
        /// <param name="OrderId"></param>
        /// <returns></returns>
        [Route("DeleteOrder")]
        [HttpDelete]
        public string DeleteOrder(int OrderId)
        {
            var orderdetails = _context.TblOrderInfo.Find(OrderId);
            if (orderdetails != null)
            {
                _context.TblOrderInfo.Remove(orderdetails);
                _context.SaveChanges();
                return "Order deleted Sucessfully.";

            }
            else
            {
                return "Order Not found.Please enter valid Order Id.";
            }

        }

        /// <summary>
        /// Method the View the order details based user details either buyer or Admin.
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        [Route("ViewOrder")]
        [HttpGet]
        public List<OrderDetails> ViewOrder(string UserName)
        {
           List<OrderDetails> lstOrder = new List<OrderDetails>();
            lstOrder = _context.OrderDetails.FromSql("UDP_GetOrderDetails {0}", UserName).ToList();
            return lstOrder;
        }


        /// <summary>
        /// Method to update the placed order details.
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        [Route("UpdateOrder")]
        [HttpPost]
        public string UpdateOrder([FromBody]TblOrderInfo order)
        {
            TblOrderInfo OrderExist = _context.TblOrderInfo.FirstOrDefault(id => id.OrderId == order.OrderId);

            if(OrderExist!=null)
            {
                OrderExist.UserName = order.UserName;
                OrderExist.EmailId = order.EmailId;
                OrderExist.Address = order.Address;
                OrderExist.UserType = order.UserType;
                OrderExist.ProductId = order.ProductId;
                OrderExist.OrderStatus = order.OrderStatus;
                _context.TblOrderInfo.Update(OrderExist);
                _context.SaveChanges();

                return "Record Updated";

            }
            else
            {
                return "Record Not Found";
            }
            
        }
    }
}